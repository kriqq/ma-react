import React from 'react';
import { List, Card } from 'antd';


const Users = (props) => {
    return(

    <List
        grid={{ gutter: 16, column: 4 }}
        dataSource={props.data}
        renderItem={item => (
            <List.Item>
                <Card title={item.first_name + " " + item.last_name} extra={<a href={`/users/${item.profile.firebase_id}`}>View Profile</a>}>
                    {"User ID: " + item.id}
                    <br></br>
                    {"Email: " + item.email}
                    <br></br>
                    {"Firebase ID: " + item.profile.firebase_id}
                    <br></br>
                    {"Phone: " + item.profile.phone}
                    <br></br>
                    {"Zipcode: " + item.profile.zipcode}
                </Card>

            </List.Item>
        )}
    />
    );
}

export default Users;