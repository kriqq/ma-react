import React from 'react';
import { Form, Input, Button } from 'antd';
import axios from 'axios';

const FormItem = Form.Item;

class UserForm extends React.Component {


    handleFormSubmit = (event, requestType, firebase_id) => {
        event.preventDefault();
        const first_name = event.target.elements.first_name.value;
        const last_name = event.target.elements.last_name.value;
        const email = event.target.elements.email.value;
        const password = event.target.elements.password.value;
        const phone = event.target.elements.phone.value;
        const zipcode = event.target.elements.zipcode.value;

        switch ( requestType ) {
            case 'post':
                return axios.post('http://127.0.0.1:8000/api/users?user=Y7oaqRjc62QnhBjvwksXcLhJTs52', {
                    first_name: first_name,
                    last_name: last_name,
                    email: email,
                    password: password,
                    phone: phone,
                    zipcode: zipcode
                })
                .catch(error => console.err(error));

            case 'put':
                return axios.put(`http://127.0.0.1:8000/api/users/${firebase_id}?user=Y7oaqRjc62QnhBjvwksXcLhJTs52`, {
                    first_name: first_name,
                    last_name: last_name,
                    email: email,
                    password: password,
                    phone: phone,
                    zipcode: zipcode
                })
                .catch(error => console.err(error));

        }
    }


    render() {
        return (
          <div>
            <Form onSubmit={(event) => this.handleFormSubmit(
                event,
                this.props.requestType,
                this.props.firebase_id
            )}
            >
              <FormItem label="First Name">
                <Input name="first_name" placeholder="Put a title here" />
              </FormItem>
              <FormItem label="Last Name">
                <Input name="last_name" placeholder="Enter some content ..." />
              </FormItem>
              <FormItem label="Email">
                <Input name="email" placeholder="Put a title here" />
              </FormItem>
              <FormItem label="Password">
                <Input name="password" placeholder="Put a title here" />
              </FormItem>
              <FormItem label="Phone">
                <Input name="phone" placeholder="Put a title here" />
              </FormItem>
              <FormItem label="Zipcode">
                <Input name="zipcode" placeholder="Put a title here" />
              </FormItem>
              <FormItem>
                <Button type="primary" htmlType="submit">
                  {this.props.buttonType}
                </Button>
              </FormItem>
            </Form>
          </div>
        );
    }

}

export default UserForm;