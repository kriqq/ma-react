import React from 'react';
import { List } from 'antd';


const Artworks = (props) => {
    return(

        <List
            itemLayout="vertical"
            size="large"
            pagination={{
            onChange: page => {
                console.log(page);
            },
            pageSize: 3,
            }}
            dataSource={props.data}
            renderItem={item => (
            <List.Item
            >



                <List.Item.Meta
                title={<a href={`/artwork/${item.id}`}>{item.title}</a>}
                description={item.description}
                />
                {item.price}
            </List.Item>
            )}
        />
    );
}

export default Artworks;