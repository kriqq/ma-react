import React from 'react';
import { List } from 'antd';


const Orders = (props) => {
    return(

        <List
            itemLayout="vertical"
            size="large"
            pagination={{
            onChange: page => {
                console.log(page);
            },
            pageSize: 3,
            }}
            dataSource={props.data}
            renderItem={item => (
            <List.Item
            >



                <List.Item.Meta
                title={<a href={`/orders/${item.id}`}>Order #{item.id}</a>}
                description={item.id}
                />
                {item.cost}
            </List.Item>
            )}
        />
    );
}

export default Orders;