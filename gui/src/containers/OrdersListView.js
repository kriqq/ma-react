import React from 'react';
import axios from 'axios';

import Orders from '../components/Order';

class OrderList extends React.Component {

    state = {
        orders: []
    }

    componentDidMount(){
        axios.get('http://127.0.0.1:8000/api/orders?user=Y7oaqRjc62QnhBjvwksXcLhJTs52')
            .then(res => {
                this.setState({
                    orders: res.data
                });
                console.log(res.data)
            })
    }

    render() {
        return(
            <Orders data={this.state.orders}/>
        )
    }
}

export default OrderList;