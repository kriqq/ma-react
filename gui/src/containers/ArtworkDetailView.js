import React from 'react';
import axios from 'axios';

import { Card } from 'antd';


class ArtworkDetail extends React.Component {

    state = {
        webart: {}
    }

    componentDidMount(){
        const artwork_id = this.props.match.params.artwork_id;
        axios.get(`http://127.0.0.1:8000/api/artwork/${artwork_id}?user=Y7oaqRjc62QnhBjvwksXcLhJTs52`)
            .then(res => {
                this.setState({
                    webart: res.data
                });
                console.log(res.data)
            })
    }

    render() {
        return(
            <Card title={this.state.webart.title+ " - " + this.state.webart.artwork_type} extra={<a href={"/artwork/"}>Back</a>}>
                {"Artwork ID: " + this.state.webart.id}
                <br></br>
                {"Artist: " + this.state.webart.artist}
                <br></br>
                {"Price: £" + this.state.webart.price}
                <br></br>
                {"Description: " + this.state.webart.description}
                <br></br>
                {"Medium: " + this.state.webart.medium}
                <br></br>
                {"Dimensions: " + this.state.webart.height_cm + " x " + this.state.webart.width_cm + " x " + this.state.webart.depth_cm + "cm"}
            </Card>

        )
    }
}

export default ArtworkDetail;