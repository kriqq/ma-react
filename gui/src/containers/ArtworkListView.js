import React from 'react';
import axios from 'axios';

import Artworks from '../components/Artwork';

class ArtworkList extends React.Component {

    state = {
        artworks: []
    }

    componentDidMount(){
        axios.get('http://127.0.0.1:8000/api/artwork?user=Y7oaqRjc62QnhBjvwksXcLhJTs52')
            .then(res => {
                this.setState({
                    artworks: res.data
                });
                console.log(res.data)
            })
    }

    render() {
        return(
            <Artworks data={this.state.artworks}/>
        )
    }
}

export default ArtworkList;