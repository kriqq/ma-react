import React from 'react';
import axios from 'axios';

import Users from '../components/User';
import UserForm from '../components/UserForm'

class UserList extends React.Component {

    state = {
        users: []
    }

    componentDidMount(){
        axios.get('http://127.0.0.1:8000/api/users?user=Y7oaqRjc62QnhBjvwksXcLhJTs52')
            .then(res => {
                this.setState({
                    users: res.data
                });
                console.log(res.data)
            })
    }

    render() {
        return(
            <div>
                <Users data={this.state.users}/>
                <br></br>
                <h2>Create a new user</h2>
                <UserForm
                    requestType="post"
                    firebase_id={null}
                    buttonType="Create User"
                />
            </div>
        )
    }
}

export default UserList;