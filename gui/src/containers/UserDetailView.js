import React from 'react';
import axios from 'axios';

import { Card } from 'antd';

import UserForm from '../components/UserForm';

class UserDetail extends React.Component {

    state = {
        webuser: {}
    }

    componentDidMount(){
        const firebase_id = this.props.match.params.firebase_id;
        axios.get(`http://127.0.0.1:8000/api/users/${firebase_id}?user=Y7oaqRjc62QnhBjvwksXcLhJTs52`)
            .then(res => {
                this.setState({
                    webuser: res.data
                });
                console.log(res.data)
            })
    }

    render() {
        return(
            <div>
                <Card title={(this.state.webuser.user && this.state.webuser.user.first_name) + " " + (this.state.webuser.user && this.state.webuser.user.last_name)} extra={<a href={"/users/"}>Back</a>}>
                    {"User ID: " + (this.state.webuser.user && this.state.webuser.user.id)}
                    <br></br>
                    {"Email: " + (this.state.webuser.user && this.state.webuser.user.email)}
                    <br></br>
                    {"Firebase ID: " + this.state.webuser.firebase_id}
                    <br></br>
                    {"Phone: " + this.state.webuser.phone}
                    <br></br>
                    {"Zipcode: " + this.state.webuser.zipcode}
                </Card>
                <br></br>
                <br></br>
                <br></br>
                <UserForm
                    requestType="put"
                    firebase_id={this.props.match.params.firebase_id}
                    buttonType="Update User"
                />
            </div>
        )
    }
}

export default UserDetail;