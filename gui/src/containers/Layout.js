import React from 'react';
import { Layout, Menu, Breadcrumb } from 'antd';
import { Link } from 'react-router-dom';

const { Header, Content, Footer } = Layout;

const CustomLayout = (props) => {
    return(
        <Layout className="layout">
        <Header>
        <div className="logo" />
        <Menu theme="dark" mode="horizontal" defaultSelectedKeys={['1']}>
            <Menu.Item><Link to="/">Home</Link></Menu.Item>
            <Menu.Item><Link to="/users/">Users</Link></Menu.Item>
            <Menu.Item><Link to="/artwork/">Artwork</Link></Menu.Item>
            <Menu.Item><Link to="/orders/">Orders</Link></Menu.Item>
        </Menu>
        </Header>
        <Content style={{ padding: '0 50px' }}>
        <Breadcrumb style={{ margin: '16px 0' }}>
            <Breadcrumb.Item><Link to="/">Home</Link></Breadcrumb.Item>
            <Breadcrumb.Item><Link to="/users/">Users</Link></Breadcrumb.Item>
            <Breadcrumb.Item><Link to="/artwork/">Artwork</Link></Breadcrumb.Item>
            <Breadcrumb.Item><Link to="/orders/">Orders</Link></Breadcrumb.Item>
        </Breadcrumb>
            <div className="site-layout-content">
                {props.children}
            </div>
        </Content>
        <Footer style={{ textAlign: 'center' }}>Mads Artwork ©2021 Created by Matthew Luka</Footer>
        </Layout>
    );
}

export default CustomLayout;