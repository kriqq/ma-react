import React from 'react';
import axios from 'axios';

import { Card } from 'antd';

class OrderDetail extends React.Component {

    state = {
        weborder: {}
    }

    componentDidMount(){
        const order_id = this.props.match.params.order_id;
        axios.get(`http://127.0.0.1:8000/api/orders/${order_id}?user=Y7oaqRjc62QnhBjvwksXcLhJTs52`)
            .then(res => {
                this.setState({
                    weborder: res.data
                });
                console.log(res.data)
            })
    }

    render() {
        return(
            <Card title={"Order #" + this.state.weborder.id} extra={<a href={"/orders/"}>Back</a>}>
                {"Order ID: " + this.state.weborder.id}
                <br></br>
                {"Customer: " + (this.state.weborder.customer && this.state.weborder.customer.first_name) + " " + (this.state.weborder.customer && this.state.weborder.customer.last_name)}
                <br></br>
                {"Piece: " + (this.state.weborder.piece && this.state.weborder.piece.title)}
                <br></br>
                {"Price: £" + this.state.weborder.cost}
                <br></br>
                {"Submitted: " + this.state.weborder.is_submitted}
                <br></br>
                {"Completed: " + this.state.weborder.is_completed}
                <br></br>
            </Card>

        )
    }
}

export default OrderDetail;