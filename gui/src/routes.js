import React from 'react';
import {Route} from 'react-router-dom';

import UserList from './containers/UserListView';
import UserDetail from './containers/UserDetailView';
import ArtworkList from './containers/ArtworkListView';
import ArtworkDetail from './containers/ArtworkDetailView';
import OrderList from './containers/OrdersListView';
import OrderDetail from './containers/OrderDetailView';

const BaseRouter = () => (
    <div>
            <Route exact path='/users/' component={UserList}/>
            <Route exact path='/users/:firebase_id' component={UserDetail}/>
            <Route exact path='/artwork/' component={ArtworkList}/>
            <Route exact path='/artwork/:artwork_id' component={ArtworkDetail}/>
            <Route exact path='/orders/' component={OrderList}/>
            <Route exact path='/orders/:order_id' component={OrderDetail}/>
    </div>


);

export default BaseRouter;